package com.itau.spring.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import com.itau.spring.model.Carro;
import com.itau.spring.repository.CarroRepository;

@Controller
public class CarroController {

	@Autowired
	CarroRepository carroRepository;
	
	@RequestMapping(path="/inserircarro", method=RequestMethod.POST) // para testar colocar no browser ---> -d "" localhost:8080/carro
	@ResponseBody   //este comando retorna a mensagem para o Body do HTML. Não é necessário quando se trabalha com APIs
	public Carro inserirCarro(@RequestBody Carro carro) {
		
		return carroRepository.save(carro);
	}
	
	@RequestMapping(path="/carrosvarios", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Carro> getVariosCarros2() {		
		return carroRepository.findAll();
	}

	@RequestMapping(path="/carro/{placa}", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Carro> getBuscaPlaca() {		
		return carroRepository.findAll();
	}
		

	@RequestMapping(path="/carro", method=RequestMethod.GET) // para testar colocar no browser ---> http://localhost:8080/carro
	@ResponseBody   //este comando retorna a mensagem para o Body do HTML. Não é necessário quando se trabalha com APIs
	public Carro getCarro() {
		
		Carro carro = new Carro();
		carro.setAno(2018);
		carro.setCor("Preto");
		carro.setId(001);
		carro.setMarca("VW");
		carro.setModelo("Fusca");
		carro.setPlaca("ABC-9999");
		
		return carro;
	}

	@RequestMapping(path="/ano", method=RequestMethod.GET) // para testar colocar no browser ---> localhost:8080/ano?ano=2015
	@ResponseBody   //este comando retorna a mensagem para o Body do HTML. Não é necessário quando se trabalha com APIs
	public Carro getAlteraAno(@RequestParam int ano) {
		
		Carro carro = new Carro();
		carro.setAno(ano);
		carro.setCor("Preto");
		carro.setId(001);
		carro.setMarca("VW");
		carro.setModelo("Fusca");
		carro.setPlaca("ABC-9999");
		
		return carro;
	}
	
	@RequestMapping(path="/cor/{id1}/{id2}", method=RequestMethod.GET) // para testar colocar no browser ---> http://localhost:8080/cor/preto
	@ResponseBody   //este comando retorna a mensagem para o Body do HTML. Não é necessário quando se trabalha com APIs
	public Carro getAlteraCor(@PathVariable(value="id") String cor) {
		
		Carro carro = new Carro();
		carro.setAno(2015);
		carro.setCor(cor);
		carro.setId(001);
		carro.setMarca("VW");
		carro.setModelo("Fusca");
		carro.setPlaca("ABC-9999");
		
		return carro;
	}
	
	@RequestMapping(path="/campos/{id1}/{id2}", method=RequestMethod.GET) // para testar colocar no browser ---> http://localhost:8080/campos/azul/1975
	@ResponseBody   //este comando retorna a mensagem para o Body do HTML. Não é necessário quando se trabalha com APIs
	public Carro getAlteraCorAno(@PathVariable(value="id1") String cor, @PathVariable(value="id2") int ano) {
		
		Carro carro = new Carro();
		carro.setAno(ano);
		carro.setCor(cor);
		carro.setId(001);
		carro.setMarca("VW");
		carro.setModelo("Fusca");
		carro.setPlaca("ABC-9999");
		
		return carro;
	}
	
	@RequestMapping(path="/carro/varios", method=RequestMethod.GET) // para testar colocar no browser ---> http://localhost:8080/carro/varios
	@ResponseBody   //este comando retorna a mensagem para o Body do HTML. Não é necessário quando se trabalha com APIs
	public ArrayList<Carro> getVariosCarros() {
		
		ArrayList<Carro> carros = new ArrayList<>();
		
		for (int i = 0; i < 5; i++) {
			Carro carro = new Carro();
			carro.setAno(2018);
			carro.setCor("Preto");
			carro.setId(i+1);
			carro.setMarca("VW");
			carro.setModelo("Fusca");
			carro.setPlaca("ABC-9999");
			
			carros.add(carro);
		}
		
		return carros;
	}

	
	@RequestMapping(path="carro/bom") // para testar colocar no browser ---> http://localhost:8080/carro/bom?modelo=Corola
	public ResponseEntity<?> eBom(@RequestParam String modelo){
		
		Carro carro = new Carro();
		carro.setModelo(modelo);
		
		if (modelo.equals("fusca")){
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(carro);
	}
	
}
