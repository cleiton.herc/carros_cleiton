package com.itau.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {

	@RequestMapping("/")   // para testar colocar no browser ---> http://localhost:8080
	@ResponseBody   //este comando retorna a mensagem para o Body do HTML. Não é necessário quando se trabalha com APIs
	public String Hello() {
		return "Hello Test";
	}
}
