package com.itau.spring.repository;

import org.springframework.data.repository.CrudRepository;
import com.itau.spring.model.Carro;

public interface CarroRepository extends CrudRepository<Carro, Integer>{
	Carro findByPlaca(String placa); //esta linha é para fazer uma busca no campo "placa"
}
